# Proyecto Node de entrenamiento con Git

## Bienvenidos

El objetivo de esta práctica es iniciar un proyecto NodeJs de cero utilzando Git.

El repositorio está vacío, a no ser por este documento y el archivo .gitignore (también vacío), así que cada uno tiene que iniciar el proyecto de cero.

Si fuiste invitado vas a poder participar. Los pasos son los siguientes:

* Clonarse el repositorio.
* Crearse un branch de feature.
* Iniciar un proyecto de Node.
* Commitear las veces que consideren necesario.
* Subir su branch de feature.
* Solicitar un merge request.

En la medida en que se vayan recibiendo merge request, irán recibiendo los comentarios pertinentes.

¡Buen día para todos!

